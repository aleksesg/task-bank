package ru.innopolis.testtaskbank.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.innopolis.testtaskbank.models.Client;
import ru.innopolis.testtaskbank.services.ClientService;

import java.util.List;

@Controller
public class AdminController {

    private final ClientService clientService;

    @Autowired
    public AdminController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("home/clients")
    public String getClientsPage(Model model) {
        List<Client> clientList = clientService.getAllClient();
        model.addAttribute("clients", clientList);
        return "clients";
    }
}

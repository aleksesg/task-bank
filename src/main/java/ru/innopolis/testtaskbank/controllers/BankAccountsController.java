package ru.innopolis.testtaskbank.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.innopolis.testtaskbank.models.BankAccount;
import ru.innopolis.testtaskbank.services.BankAccountService;

import java.util.List;

@Controller
public class BankAccountsController {

    private final BankAccountService accountService;
    @Autowired
    public BankAccountsController(BankAccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("home/accounts")
    public String getAccountsPage(Model model) {
        List<BankAccount> accounts = accountService.getAllBankAccount();
        model.addAttribute("accounts", accounts);
        return "accounts";
    }

    @GetMapping("home/account/account")
    public String getAccountPage(Model model, @PathVariable("account-id") Integer id) {
        BankAccount account = accountService.getById(id);
        model.addAttribute("account", account);
        return "account";
    }

}

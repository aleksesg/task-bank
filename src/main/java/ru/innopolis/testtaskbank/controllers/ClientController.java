package ru.innopolis.testtaskbank.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.innopolis.testtaskbank.forms.BankAccountForm;
import ru.innopolis.testtaskbank.forms.ClientForm;
import ru.innopolis.testtaskbank.models.BankAccount;
import ru.innopolis.testtaskbank.models.Client;
import ru.innopolis.testtaskbank.services.ClientService;

import java.util.List;

@Controller
public class ClientController {
    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }


    @GetMapping("/home/client")
    public String getClientPage(Model model, @AuthenticationPrincipal UserDetails userDetails) {
        Client client = clientService.getByLogin(userDetails.getUsername());
        model.addAttribute("client", client);
        return "client";
    }

    @GetMapping("/home/client/update")
    public String getClientUpdatePage(Model model, @AuthenticationPrincipal UserDetails userDetails) {
        Client client = clientService.getByLogin(userDetails.getUsername());
        model.addAttribute("client", client);
        return "client_change";
    }

    @GetMapping("/home/client/accounts")
    public String getAccountsByClient(Model model, @AuthenticationPrincipal UserDetails userDetails) {
        Client client = clientService.getByLogin(userDetails.getUsername());
        List<BankAccount> accountsByClient = clientService.getAccountsByClient(client.getId());
        model.addAttribute("accountsByClient", accountsByClient);
        return "client_bank_accounts";
    }

    @PostMapping("/home/client/update")
    public String updateClient(@ModelAttribute("modelClient") ClientForm clientForm,@AuthenticationPrincipal UserDetails userDetails) {
        Client client = clientService.getByLogin(userDetails.getUsername());
        clientService.update(clientForm, client.getId());
        return "redirect:/home/client";
    }

    @PostMapping("/home/client/add-account")
    public String creatBankAccountsByClient(BankAccountForm accountForm, @AuthenticationPrincipal UserDetails userDetails) {
        Client client = clientService.getByLogin(userDetails.getUsername());
        clientService.addAccountToClient(client, accountForm);
        return "redirect:/home/client/accounts";
    }

    @PostMapping("home/client/accounts/{account-id}/delete")
    public String deleteAccountByClient(@PathVariable("account-id") Integer accountId) {
        clientService.deleteAccountByClient(accountId);
        return "redirect:/home/client/accounts";
    }

    @PostMapping("home/client/delete")
    public String delete(@AuthenticationPrincipal UserDetails userDetails) {
        Client client = clientService.getByLogin(userDetails.getUsername());
        clientService.delete(client.getId());
        return "redirect:/signIn";
    }
    @PostMapping("home/client/exit")
    public String exit() {
        return "redirect:/signIn";
    }
}

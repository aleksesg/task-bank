package ru.innopolis.testtaskbank.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.innopolis.testtaskbank.forms.SignUpClientForm;
import ru.innopolis.testtaskbank.services.SignUpService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/signUp")
public class SignUpController {

    private final SignUpService signUpService;

    @GetMapping
    public String getSignUpPage() {
        return "signUp";
    }

    @PostMapping
    public String SignUpSet(SignUpClientForm form) {
        signUpService.signUpClient(form);
        return "redirect:/signIn";
    }
}

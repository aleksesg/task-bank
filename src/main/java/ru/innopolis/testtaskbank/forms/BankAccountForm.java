package ru.innopolis.testtaskbank.forms;

import lombok.Builder;
import lombok.Data;
import ru.innopolis.testtaskbank.models.Client;

@Data
@Builder
public class BankAccountForm {
    private Integer balance;
    private Client client;
}

package ru.innopolis.testtaskbank.forms;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClientForm {
    private String firstName;
    private String lastName;
    private Integer day;
    private Integer month;
    private Integer year;
    private String email;
    private String login;
    private String password;
}

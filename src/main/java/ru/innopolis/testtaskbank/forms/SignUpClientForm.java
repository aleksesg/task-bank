package ru.innopolis.testtaskbank.forms;

import lombok.Data;

@Data
public class SignUpClientForm {
    private String firstName;
    private String lastName;
    private Integer day;
    private Integer month;
    private Integer year;
    private String email;
    private String login;
    private String password;
}

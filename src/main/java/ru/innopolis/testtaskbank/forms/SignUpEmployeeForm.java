package ru.innopolis.testtaskbank.forms;

import lombok.Data;

@Data
public class SignUpEmployeeForm {
    private String firstName;
    private String lastName;
    private String email;
    private String login;
    private String password;
}

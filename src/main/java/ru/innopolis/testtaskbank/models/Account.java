package ru.innopolis.testtaskbank.models;

public enum Account {
    DEPOSIT_ACCOUNT ("Deposit account"),
    CREDIT_CARD ("Credit card"),
    TIME_DEPOSIT("Time deposit");

    private final String nameAccount;

    Account(String nameAccount) {
        this.nameAccount = nameAccount;
    }

    public String getNameAccount() {
        return nameAccount;
    }
}

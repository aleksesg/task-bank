package ru.innopolis.testtaskbank.models;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Builder
@Entity
@Table(name = "bank_account")
public class BankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer balance = 0;

    @ManyToOne
    @JoinColumn(name = "client_id", nullable = false)
    private Client client;
}

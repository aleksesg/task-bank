package ru.innopolis.testtaskbank.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Builder
@Entity
@Table(name = "clients")
public class Client {
    public enum Role {
        CLIENT,
        ADMIN;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String firstName;
    private String lastName;
    private LocalDate birthday;

    @Column(unique = true)
    private String email;

    @Column(unique = true)
    private String login;
    private String password;

    @OneToMany(mappedBy = "client")
    private List<BankAccount> accountList;

    @Enumerated(value = EnumType.STRING)
    private Role role;
}

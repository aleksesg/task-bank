package ru.innopolis.testtaskbank.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.innopolis.testtaskbank.models.BankAccount;

import java.util.List;

public interface BankAccountRepository extends JpaRepository<BankAccount, Integer> {
     List<BankAccount> findAllByClient_Id(Integer id);
}

package ru.innopolis.testtaskbank.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.innopolis.testtaskbank.models.History;

public interface HistoryRepository extends JpaRepository<History, Integer> {
}

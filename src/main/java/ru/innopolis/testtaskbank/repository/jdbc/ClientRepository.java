package ru.innopolis.testtaskbank.repository.jdbc;

import org.springframework.stereotype.Repository;
import ru.innopolis.testtaskbank.models.Client;

import java.util.List;

@Repository
public interface ClientRepository {
    List<Client> findAll();

    void save(Client user);

    Client findById(Integer id);

    void delete (Integer id);

    public List<Client> findClientByLogin(String login, String password);
}

package ru.innopolis.testtaskbank.repository.jdbc;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.innopolis.testtaskbank.models.Client;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Component
public class ClientRepositoryJdbcImpl implements ClientRepository {

    //language=SQL
    private static final String SQL_INSERT = "insert into clients(first_name, last_name, birthday, login, email, password) values (?,?,?,?,?,?)";

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from clients order by id";

    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from clients where id = ?";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from clients where id = ?";

    //language=SQL
    private static final String SQL_SELECT_CLIENT_BY_LOGIN = "select * from clients where login = ? AND password=?";


    private final JdbcTemplate jdbcTemplate;

    public ClientRepositoryJdbcImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final RowMapper<Client> userRowMapper = (row, rowNumber) -> {

        Integer id = row.getInt("id");
        String firstName = row.getString("first_name");
        String lastName = row.getString("last_name");
        Date date = row.getDate("birthday");
        LocalDate birthday = row.getObject("birthday", LocalDate.class);

        Client client = Client.builder()
                .id(id)
                .firstName(firstName)
                .lastName(lastName)
                .birthday(birthday)
                .build();
        return client;
    };

    private static final RowMapper<Client> userRowMapperAll = (row, rowNumber) -> {

        Integer id = row.getInt("id");
        String firstName = row.getString("first_name");
        String lastName = row.getString("last_name");
        Date date = row.getDate("birthday");
        LocalDate birthday = row.getObject("birthday", LocalDate.class);
        String login = row.getString("login");
        String email = row.getString("email");

        Client client = Client.builder()
                .id(id)
                .firstName(firstName)
                .lastName(lastName)
                .birthday(birthday)
                .login(login)
                .email(email)
                .build();
        return client;
    };

    @Override
    public List<Client> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, userRowMapper);
    }

    @Override
    public List<Client> findClientByLogin(String login, String password) {
        return jdbcTemplate.query(SQL_SELECT_CLIENT_BY_LOGIN, userRowMapperAll, login, password);
    }


    @Override
    public void save(Client client) {
        jdbcTemplate.update(SQL_INSERT, client.getFirstName(), client.getLastName(), client.getBirthday(), client.getLogin(), client.getLogin(), client.getPassword());
    }

    @Override
    public Client findById(Integer id) {
        return jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, userRowMapperAll, id);
    }

    @Override
    public void delete(Integer id) {
        jdbcTemplate.update(SQL_DELETE_BY_ID, id);
    }
}

package ru.innopolis.testtaskbank.security.details;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.innopolis.testtaskbank.models.Client;
import ru.innopolis.testtaskbank.repository.ClientRepository;

@RequiredArgsConstructor
@Component
public class UserDetailsServiceImpl implements UserDetailsService {
    private final ClientRepository clientRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Client client = clientRepository.findByLogin(login)
                .orElseThrow(() -> new UsernameNotFoundException("Client not found"));
        return new UsersDetailsImpl(client);
    }
}

package ru.innopolis.testtaskbank.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.innopolis.testtaskbank.models.BankAccount;
import ru.innopolis.testtaskbank.repository.BankAccountRepository;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

@Service
public class BankAccountDataBaseService implements BankAccountService {

    @Autowired
    BankAccountRepository bankAccountRepository;

    @Override
    public BankAccount getById(Integer id) {
        return bankAccountRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Bank account: %s - not found", id)));
    }

    @Override
    public List<BankAccount> getAllBankAccount() {
        Iterable<BankAccount> bankAccountIterable = bankAccountRepository.findAll();
        List<BankAccount> bankAccounts = new ArrayList<>();
        bankAccountIterable.forEach(bankAccounts::add);
        return bankAccounts;
    }
}

package ru.innopolis.testtaskbank.services;

import ru.innopolis.testtaskbank.models.BankAccount;

import java.util.List;

public interface BankAccountService {
    BankAccount getById(Integer id);

    List<BankAccount> getAllBankAccount();

}

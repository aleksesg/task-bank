package ru.innopolis.testtaskbank.services;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.innopolis.testtaskbank.forms.BankAccountForm;
import ru.innopolis.testtaskbank.forms.ClientForm;
import ru.innopolis.testtaskbank.models.BankAccount;
import ru.innopolis.testtaskbank.models.Client;
import ru.innopolis.testtaskbank.repository.BankAccountRepository;
import ru.innopolis.testtaskbank.repository.ClientRepository;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.List;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class ClientDataBaseService implements ClientService {

    private final ClientRepository clientRepository;
    private final BankAccountRepository bankAccountRepository;

    @PostConstruct
    void init() {
        List<Client> clientList = getAllClient();
        long countClient = clientList.size();
        if (countClient < 5) {
            for (int i = 1; i <= 5; i++) {
                Client client = Client.builder()
                        .firstName("FirstTest " + i)
                        .lastName("LastTest " + i)
                        .birthday(LocalDate.of(2000 - i, i, i))
                        .login("LoginTest " + i)
                        .role(Client.Role.CLIENT)
                        .email(i + "test@test.com")
                        .password("test" + i)
                        .build();
                clientRepository.save(client);
                for (int j = 0; j <= 5; j++) {
                    BankAccount account = BankAccount.builder()
                            .client(client)
                            .balance(10 * j)
                            .build();
                    bankAccountRepository.save(account);
                }
            }
        }
    }

    @Override
    public Client getById(Integer id) {
        return clientRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Client with id %s not found", id)));
    }

    @Override
    public List<Client> getAllClient() {
        return clientRepository.findAll();
    }

    @Override
    public Client createClient(ClientForm clientForm) {
        Client client = Client.builder()
                .firstName(clientForm.getFirstName())
                .lastName(clientForm.getLastName())
                .birthday(LocalDate.of(clientForm.getYear(), clientForm.getMonth(), clientForm.getDay()))
                .login(clientForm.getLogin())
                .email(clientForm.getEmail())
                .role(Client.Role.CLIENT)
                .password(clientForm.getPassword())
                .build();
        return clientRepository.save(client);
    }

    @Override
    public void delete(Integer id) {
        clientRepository.deleteById(id);
    }

    @Override
    public List<BankAccount> getAccountsByClient(Integer clientId) {
        return bankAccountRepository.findAllByClient_Id(clientId);
    }

    @Override
    public BankAccount addAccountToClient(Client clientId, BankAccountForm accountForm) {

        BankAccount account = BankAccount.builder()
                .client(clientId)
                .balance((accountForm.getBalance() == null) ? 0 : accountForm.getBalance())
                .build();
        return bankAccountRepository.save(account);
    }

    @Override
    public void deleteAccountByClient(Integer accountId) {
        bankAccountRepository.deleteById(accountId);
    }

    @Override
    public Client getByLogin(String username) {
        return clientRepository.findByLogin(username)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Client with login %s not found", username)));
    }

    @Override
    public Client update(ClientForm clientForm, Integer id) {
        Client client = clientRepository.getById(id);
        client.setFirstName(clientForm.getFirstName());
        client.setLastName(clientForm.getLastName());
        client.setBirthday(LocalDate.of(clientForm.getYear(), clientForm.getMonth(), clientForm.getDay()));
        client.setLogin(clientForm.getLogin());
        client.setEmail(clientForm.getEmail());
        clientRepository.save(client);
        return client;
    }
}

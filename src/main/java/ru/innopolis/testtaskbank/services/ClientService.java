package ru.innopolis.testtaskbank.services;

import ru.innopolis.testtaskbank.forms.BankAccountForm;
import ru.innopolis.testtaskbank.forms.ClientForm;
import ru.innopolis.testtaskbank.models.BankAccount;
import ru.innopolis.testtaskbank.models.Client;

import java.util.List;

public interface ClientService {
    Client getById(Integer id);

    List<Client> getAllClient();

    Client createClient(ClientForm clientForm);

    void delete(Integer id);

    List<BankAccount> getAccountsByClient(Integer clientId);

    BankAccount addAccountToClient(Client clientId, BankAccountForm accountForm);

    void deleteAccountByClient(Integer accountId);

    Client getByLogin(String username);

    Client update(ClientForm clientForm, Integer id);
}

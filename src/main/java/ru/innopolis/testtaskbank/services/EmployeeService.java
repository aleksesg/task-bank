package ru.innopolis.testtaskbank.services;

import ru.innopolis.testtaskbank.models.Employee;

import java.util.List;
import java.util.UUID;

public interface EmployeeService {
    Employee getById(UUID id);

    List<Employee> getAllEmployee();

    Employee createEmployee(Employee employee);

    Employee updateEmployee(UUID id, Employee employee);

    void delete(UUID id);
}

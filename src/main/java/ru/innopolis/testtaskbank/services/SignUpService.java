package ru.innopolis.testtaskbank.services;

import ru.innopolis.testtaskbank.forms.SignUpClientForm;

public interface SignUpService {
    void signUpClient(SignUpClientForm form);
}

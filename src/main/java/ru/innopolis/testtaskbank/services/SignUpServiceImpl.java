package ru.innopolis.testtaskbank.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.innopolis.testtaskbank.forms.SignUpClientForm;
import ru.innopolis.testtaskbank.models.Client;
import ru.innopolis.testtaskbank.repository.ClientRepository;

import java.time.LocalDate;

@RequiredArgsConstructor
@Service
public class SignUpServiceImpl implements SignUpService {

    private final PasswordEncoder passwordEncoder;
    private final ClientRepository clientRepository;

    @Override
    public void signUpClient(SignUpClientForm form) {
        Client client = Client.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .birthday(LocalDate.of(form.getYear(), form.getMonth(), form.getDay()))
                .login(form.getLogin())
                .email(form.getEmail())
                .role(Client.Role.CLIENT)
                .password(passwordEncoder.encode(form.getPassword()))
                .build();
        clientRepository.save(client);
    }
}

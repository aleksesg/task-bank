package ru.innopolis.testtaskbank;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.web.client.RestTemplate;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TaskBankApplicationTests {

    @LocalServerPort
    protected int port;

    protected String baseUrl = "http://localhost:";
    protected static RestTemplate restTemplate = new RestTemplate();

    @BeforeEach
    public void setUp() {
        baseUrl = baseUrl.concat(port + "");
    }


}

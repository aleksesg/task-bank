package ru.innopolis.testtaskbank.services;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import ru.innopolis.testtaskbank.TaskBankApplicationTests;
import ru.innopolis.testtaskbank.forms.BankAccountForm;
import ru.innopolis.testtaskbank.forms.ClientForm;
import ru.innopolis.testtaskbank.models.BankAccount;
import ru.innopolis.testtaskbank.models.Client;

import java.util.List;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

class BankAccountDataBaseServiceTest extends TaskBankApplicationTests {

    @Autowired
    private ClientDataBaseService clientDataBaseService;
    @Autowired
    private BankAccountService bankAccountService;

    ClientForm creatTestClientForm() {
        return ClientForm.builder()
                .firstName("Ivan")
                .lastName("Ivanov")
                .day(1)
                .month(1)
                .year(2001)
                .login("Ivan")
                .email("ivan@mail.ru")
                .password("ivan")
                .build();
    }

    @Test
    void getById() {
        ClientForm clientForm = creatTestClientForm();
        Client testClient = clientDataBaseService.createClient(clientForm);
        BankAccountForm testedAccountForm = BankAccountForm.builder()
                .balance(10)
                .client(testClient)
                .build();

        BankAccount account = clientDataBaseService.addAccountToClient(testClient, testedAccountForm);
        BankAccount getAccount = bankAccountService.getById(account.getId());

        assertEquals(account.getBalance(), getAccount.getBalance());
        assertEquals(account.getClient().getId(), getAccount.getClient().getId());
        assertEquals(account.getId(), getAccount.getId());

        clientDataBaseService.deleteAccountByClient(getAccount.getId());
        clientDataBaseService.delete(testClient.getId());
    }

    @Test
    void getAllBankAccount() {
        ClientForm clientForm = creatTestClientForm();
        Client testClient = clientDataBaseService.createClient(clientForm);
        BankAccountForm testedAccountForm = BankAccountForm.builder()
                .balance(10)
                .client(testClient)
                .build();

        BankAccount account = clientDataBaseService.addAccountToClient(testClient, testedAccountForm);

        List<BankAccount> accounts = bankAccountService.getAllBankAccount();
        assertNotNull(accounts);

        clientDataBaseService.deleteAccountByClient(account.getId());
        clientDataBaseService.delete(testClient.getId());
    }

    @Test
    void testGetByIdNonExistingId() {
        Integer id = -1;

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> bankAccountService.getById(id),
                "Excepted to return throw Exception"
        );
        assertEquals(format("404 NOT_FOUND \"Bank account: %s - not found\"", id), exception.getMessage());
    }
}
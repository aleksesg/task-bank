package ru.innopolis.testtaskbank.services;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import ru.innopolis.testtaskbank.TaskBankApplicationTests;
import ru.innopolis.testtaskbank.forms.BankAccountForm;
import ru.innopolis.testtaskbank.forms.ClientForm;
import ru.innopolis.testtaskbank.models.BankAccount;
import ru.innopolis.testtaskbank.models.Client;

import java.time.LocalDate;
import java.util.List;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

class ClientDataBaseServiceTest extends TaskBankApplicationTests {

    @Autowired
    private ClientDataBaseService clientDataBaseService;
    @Autowired
    private BankAccountService bankAccountService;

    @Test
    void testGetByIdNonExistingId() {
        Integer id = -1;

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> clientDataBaseService.getById(id),
                "Excepted to return throw Exception"
        );
        assertEquals(format("404 NOT_FOUND \"Client with id %s not found\"", id), exception.getMessage());
    }

    @Test
    void testGetById() {
        ClientForm clientForm = creatTestClientForm();

        Client createdClient = clientDataBaseService.createClient(clientForm);

        Client testClient = clientDataBaseService.getById(createdClient.getId());
        assertNotNull(createdClient.getId());
        assertEquals(createdClient.getFirstName(), testClient.getFirstName());
        assertEquals(createdClient.getLastName(), testClient.getLastName());
        assertEquals(createdClient.getLogin(), testClient.getLogin());
        assertEquals(createdClient.getEmail(), testClient.getEmail());
        assertEquals(createdClient.getPassword(), testClient.getPassword());
        assertEquals(createdClient.getBirthday(), testClient.getBirthday());
        clientDataBaseService.delete(createdClient.getId());
    }

    @Test
    void testGetAllClient() {
        List<Client> clientList = clientDataBaseService.getAllClient();
        assertNotNull(clientList);
    }

    ClientForm creatTestClientForm() {
        return ClientForm.builder()
                .firstName("Ivan")
                .lastName("Ivanov")
                .day(1)
                .month(1)
                .year(2001)
                .login("Ivan")
                .email("ivan@mail.ru")
                .password("ivan")
                .build();
    }

    @Test
    void testCreateClient() {
        ClientForm clientForm = creatTestClientForm();
        Client testClient = Client.builder()
                .firstName(clientForm.getFirstName())
                .lastName(clientForm.getLastName())
                .birthday(LocalDate.of(clientForm.getYear(), clientForm.getMonth(), clientForm.getDay()))
                .login(clientForm.getLogin())
                .email(clientForm.getEmail())
                .password(clientForm.getPassword())
                .role(Client.Role.CLIENT)
                .build();
        Client createdClient = clientDataBaseService.createClient(clientForm);
        assertNotNull(createdClient.getId());
        assertEquals(createdClient.getFirstName(), testClient.getFirstName());
        assertEquals(createdClient.getLastName(), testClient.getLastName());
        assertEquals(createdClient.getLogin(), testClient.getLogin());
        assertEquals(createdClient.getEmail(), testClient.getEmail());
        assertEquals(createdClient.getPassword(), testClient.getPassword());
        assertEquals(createdClient.getBirthday(), testClient.getBirthday());
        clientDataBaseService.delete(createdClient.getId());
    }

    @Test
    void testDelete() {
        ClientForm clientForm = creatTestClientForm();
        Client client = clientDataBaseService.createClient(clientForm);
        Integer id = client.getId();
        clientDataBaseService.delete(id);
        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> clientDataBaseService.getById(id),
                "Excepted to return throw Exception"
        );
        assertEquals(format("404 NOT_FOUND \"Client with id %s not found\"", id), exception.getMessage());
    }

    @Test
    void testGetAccountsByClient() {
        ClientForm clientForm = creatTestClientForm();
        Client testClient = clientDataBaseService.createClient(clientForm);
        BankAccountForm testedAccountForm = BankAccountForm.builder()
                .balance(10)
                .client(testClient)
                .build();

        BankAccount account = clientDataBaseService.addAccountToClient(testClient, testedAccountForm);

        List<BankAccount> accountsByClient = clientDataBaseService.getAccountsByClient(testClient.getId());
        assertEquals(1, accountsByClient.size());
        BankAccount getAccount = accountsByClient.get(0);
        assertEquals(account.getBalance(), getAccount.getBalance());
        assertEquals(account.getClient().getId(), getAccount.getClient().getId());
        assertEquals(account.getId(), getAccount.getId());

        clientDataBaseService.deleteAccountByClient(getAccount.getId());
        clientDataBaseService.delete(testClient.getId());
    }

    @Test
    void testAddAccountByClient() {
        ClientForm clientForm = creatTestClientForm();
        Client testClient = clientDataBaseService.createClient(clientForm);
        BankAccountForm testedAccountForm = BankAccountForm.builder()
                .balance(10)
                .client(testClient)
                .build();
        BankAccount account = clientDataBaseService.addAccountToClient(testClient, testedAccountForm);
        List<BankAccount> accountsByClient = clientDataBaseService.getAccountsByClient(testClient.getId());
        BankAccount getAccount = accountsByClient
                .stream()
                .filter(acc -> acc.getId().equals(account.getId()))
                .findFirst()
                .get();
        assertEquals(account.getBalance(), getAccount.getBalance());
        assertEquals(account.getClient().getId(), getAccount.getClient().getId());
        assertEquals(account.getId(), getAccount.getId());

        clientDataBaseService.deleteAccountByClient(getAccount.getId());
        clientDataBaseService.delete(testClient.getId());

    }

    @Test
    void testDeleteAccountByClient() {
        ClientForm clientForm = creatTestClientForm();

        Client client = clientDataBaseService.createClient(clientForm);

        BankAccountForm testAccountForm = BankAccountForm.builder()
                .balance(10)
                .client(client)
                .build();

        BankAccount testAccount = clientDataBaseService.addAccountToClient(client, testAccountForm);

        clientDataBaseService.deleteAccountByClient(testAccount.getId());

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> bankAccountService.getById(testAccount.getId()),
                format("Expected to return nothing and throw exception, since user with id %s was deleted",
                        testAccount)
        );

        assertEquals(format("404 NOT_FOUND \"Bank account: %s - not found\"", testAccount.getId()), exception.getMessage());
        clientDataBaseService.delete(client.getId());
    }

}